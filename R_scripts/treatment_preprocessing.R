library(minfi)
library(ChAMP)
library(dendextend)
library(ggplot2)
library(plyr)
library(dplyr)
library("IlluminaHumanMethylationEPICanno.ilm10b2.hg19")
library(stringr)
library(Rmisc)
library(car)
library(stats)
source("./svdWider.R")
source("./qualityControl.R")
set.seed(5)

contTable <- function(){
  #maternal_contamination is a table of CpG sites and threshold values
  mat_cont <- data.frame(maternal_contamination[[2]],
                         row.names=maternal_contamination[[1]])
  mat_cont <- merge(mat_cont, norm_unfiltered, by="row.names")
  colnames(mat_cont)[c(1,2)] <- c("Name", "Thresh")
  return(mat_cont)
}

contList <- function(){
  cont_samples <- list()
  for (i in 1:nrow(mat_cont)){
    cont_samples[[i]] <- list()
    for (j in 3:ncol(mat_cont)){
      if (mat_cont[[i,j]]>mat_cont[[i,"Thresh"]]){
        cont_samples[[i]] <- append(cont_samples[[i]], colnames(mat_cont)[[j]])
      }
    }
  }
  names(cont_samples) <- mat_cont[["Name"]]
  return(cont_samples)
}

cellProportions <- function(){
  cell_prop <- list()
  for (i in 1:length(pd_subset_lst)){
    beta_subset <- norm_betas[,colnames(norm_betas) %in% pd_subset_lst[[i]][["Sample_Name"]]]
    cell_prop[[i]] <- champ.refbase(beta=beta_subset,
                                    arraytype="450K")$CellFraction
  }
  names(cell_prop) <- treatments
  return(cell_prop)
}

addProportions <- function(){
  for (cell_type in row.names(cell_prop_table)){
    for (treatment in colnames(cell_prop_table)){
      cell_prop_table[cell_type, treatment] <- makeMedianIqr(cell_prop[[treatment]][,cell_type])
    }
  }
  return(cell_prop_table)
}

makeEmptyTable <- function(rows, colms){
  table <- data.frame(row.names=rows)
  table[1:length(colms)] <- NA
  colnames(table) <- colms
  return(table)
}

#--------------------------------------------------------------------------------------------
unfiltered_data <- champ.load(directory = getwd(),
                              method="ChAMP",
                              methValue="B",
                              autoimpute=TRUE,
                              filterDetP=FALSE,
                              ProbeCutoff=1,
                              SampleCutoff=1,
                              detPcut=NULL,
                              filterBeads=FALSE,
                              beadCutoff=1,
                              filterNoCG=TRUE,
                              filterSNPs=FALSE,
                              population=NULL,
                              filterMultiHit=FALSE,
                              filterXY=FALSE,       #Filter out XY or not
                              force=FALSE,
                              arraytype="EPIC")

norm_unfiltered <- champ.norm(beta = unfiltered_data$beta,
                              resultsDir = "./CHAMP_Normalization/",
                              method = "BMIQ",
                              plotBMIQ = FALSE,
                              arraytype = "EPIC")
#---Maternal contamination
mat_cont <- contTable()
cont_samples <- contList()

#---Load data from sample sheet and IDAT files---
raw_data <- champ.load(directory = getwd(),
                       method="ChAMP",
                       methValue="B",
                       autoimpute=TRUE,
                       filterDetP=TRUE,
                       ProbeCutoff=0,
                       SampleCutoff=0.1,
                       detPcut=0.01,
                       filterBeads=TRUE,
                       beadCutoff=0.05,
                       filterNoCG=TRUE,
                       filterSNPs=TRUE,
                       population=NULL,
                       filterMultiHit=TRUE,
                       filterXY=TRUE,       #Filter out XY or not
                       force=FALSE,
                       arraytype="EPIC")

#----CpG distribution---
CpG.GUI(CpG = rownames(raw_data$beta),
        arraytype = "EPIC")

#---Normalisation---
norm_betas <- champ.norm(beta = raw_data$beta,
                         resultsDir = "./CHAMP_Normalization/",
                         method = "BMIQ",
                         plotBMIQ = FALSE,
                         arraytype = "EPIC")

#---SVD analysis for batch effects--- Run before and after correction with Combat
svdPlot(beta = norm_betas, #Modified version of champ.SVD
         rgSet = raw_data$rgSet,
         pd = test,
         PDFplot = FALSE,
         resultsDir = "./CHAMP_SVDimages_beforeCombat/")

#---Other celltypes
cell_prop <- cellProportions()
cell_prop_table <- makeEmptyTable(colnames(cell_prop[[1]]), treatments)
cell_prop_table <- addProportions()

#---Correct for batch effects---
corrected_betas <- champ.runCombat(beta=norm_betas,
                                   pd=raw_data$pd,
                                   variablename="Treatment",
                                   batchname=c("Slide", "Array"),
                                   logitTrans=TRUE)
#---Modified version of champ.SVD
test <- pd_data
colnames(test) <- c("Sample_Name", 
                    "Sample_ID", 
                    "Slide", 
                    "Array", 
                    "Sample group",
                    "Siblings",
                    "Mat. atopy",
                    "Pat. atopy",
                    "Birth week",
                    "Mat. age",
                    "Treatm. weeks",
                    "Caesarean",
                    "Sex",
                    "Weight",
                    "Height",
                    "Breastfeeding",
                    "Antibiotics",
                    "Family",
                    "Pets",
                    "Smoking",
                    "Naive cells",
                    "Memory cells")

svdPlot(beta = corrected_betas, 
         rgSet = raw_data$rgSet,
         pd = test,
         PDFplot = FALSE,
         resultsDir = "./CHAMP_SVDimages_afterCombat/")

minfi::densityPlot(dat = raw_data$beta, 
                   #sampGroups = raw_data$pd$Treatment, 
                   main = "Hello", 
                   xlab = "Beta", 
                   legend = FALSE,
                   pal = c("royalblue4", "royalblue1", "deepskyblue", "lightblue1"))

minfi::densityPlot(dat = norm_betas, 
                   #sampGroups = raw_data$pd$Treatment, 
                   main = "Hello", 
                   xlab = "Beta", 
                   legend = FALSE,
                   pal = c("royalblue1", "deepskyblue", "lightblue1"))
minfi::densityPlot(dat = corrected_betas, 
                   #sampGroups = raw_data$pd$Treatment, 
                   main = "Hello", 
                   xlab = "Beta", 
                   legend = FALSE,
                   pal = c("deepskyblue", "lightblue1"))

minfi::mdsPlot(raw_data$beta, 
        numPositions = 1000, 
        sampGroups = pd_data$Sex, 
        pal = c("royalblue4", "deepskyblue"), 
        legendPos = "topright")

