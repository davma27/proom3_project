library(ChAMP)
library(readr)
library(ggplot2)
source("svdPlot.R")

removeXY <- function(beta){
  data("probe.features")
  no_X <- probe.features[probe.features$CHR!="X",]
  no_XY <- no_X[no_X$CHR!="Y",]
  beta <- beta[row.names(beta) %in% row.names(no_XY),]
  return(beta)
}

beta_raw <- removeXY(beta_raw)

imputed <- champ.impute(beta=beta_raw,
                        pd=pd_raw,
                        method="Combine",
                        k=5,
                        ProbeCutoff=0.2,
                        SampleCutoff=0.15)

imputed_beta <- imputed$beta
pd <- imputed$pd
pd <- as.data.frame(pd)

champ.QC(beta=imputed_beta,
         pheno=pd$Sample_Group,
         mdsPlot=TRUE,
         densityPlot=TRUE,
         dendrogram=TRUE,
         PDFplot=TRUE,
         Rplot=TRUE,
         resultsDir="./QC_raw/")

champ.QC(beta=imputed_beta,
         pheno=pd$Sample_Type,
         mdsPlot=TRUE,
         densityPlot=TRUE,
         dendrogram=TRUE,
         PDFplot=TRUE,
         Rplot=TRUE,
         resultsDir="./QC_raw_ST/")

norm_beta <- champ.norm(beta=imputed_beta,
                        method="BMIQ",
                        plotBMIQ=FALSE,
                        arraytype="EPIC",
                        cores=3)
rm(imputed_beta)
saveRDS(norm_beta, "norm_beta.rds")

champ.QC(beta=norm_beta,
         pheno=pd$Pollen_Season,
         mdsPlot=TRUE,
         densityPlot=TRUE,
         dendrogram=TRUE,
         PDFplot=TRUE,
         Rplot=TRUE,
         resultsDir="./QC_norm/")

svdPlot(beta=norm_beta,
          pd=pd[,2:(ncol(pd)-1)],
          PDFplot=TRUE,
          Rplot=TRUE,
          resultsDir="./SVD_norm/")

corr_beta <- champ.runCombat(beta=norm_beta,
                                  pd=pd,
                                  variablename="sample_group",
                                  batchname="project_id",
                                  logitTrans=TRUE)
rm(norm_beta)
saveRDS(corr_beta, "corr_beta.rds")

champ.QC(beta=corr_beta,
         pheno=pd$sample_group,
         mdsPlot=TRUE,
         densityPlot=TRUE,
         dendrogram=FALSE,
         PDFplot=TRUE,
         Rplot=FALSE,
         resultsDir="./QC_corr/")

champ.SVD(beta=corr_beta,
          pd=pd[,2:ncol(pd)],
          PDFplot=TRUE,
          Rplot=FALSE,
          resultsDir="./SVD_corr/")
