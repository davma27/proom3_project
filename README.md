# Code description

#### Section 1 (Author: Elin Olsson)
treatment_preprocessing.R reads the raw data (IDAT files and pd file) and converts to a raw beta matrix and performs normalization and correction of beta values. It also performs quality control (test for maternal contamination, test for cell types).

treatment_analysis.R performs differential methylation analysis and KEGG pathway enrichment analysis.

treatment_desc_data.R performs statistical calculations based on the data in pd_data.csv and makes a table of the results.

disease_preprocessing.R removes sex chromosomes, filters NA probes and samples, imputes remaining NAs, normalizes and does batch effect correction.

disease_analysis.R performs differential methylation analysis.

enrichment.R performs a Spearman test and a Fisher's exact test and puts the results into a table. It also makes plots of the fold enrichments and the correlations.

#### Section 2 (Author: David Martínez)
1_proom3.R includes a general DNA methylation analysis pipeline by the ChAMP R package (pre-processing, normalization, batch effect correction, identification of DMPs, DMRs, and DMBs; GSEA, interaction hotspots, CNV analysis, and cell type heterogeneity).

2_proom3.R applies a MODifieR module detection worfklow from a list of DMPs, followed by module interpretation via pathway enrichment, and further network analyses.

3_proom3.R applies a MODifieR module detection worfklow from raw methylation files, followed by module interpretation via pathway enrichment, and further network and risk factor association analyses.

4_proom3.R includes the code to create bar plots for DMP enrichment in allergy.
